package com.mitocode.controller;

import java.io.IOException;
import java.net.URI;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.mitocode.exception.ModeloNotFoundException;
import com.mitocode.model.Cliente;
import com.mitocode.model.Comida;
import com.mitocode.model.Usuario;
import com.mitocode.service.IClienteService;
import com.mitocode.service.IUsuarioService;

@RestController
@RequestMapping("/clientes")
public class ClienteController {

	@Autowired
	private IUsuarioService usuarioIService;
	
	
	@Autowired
	private IClienteService service;
	
	@GetMapping
	public ResponseEntity<List<Cliente>> listar(){
		List<Cliente> lista = service.listar();
		return new ResponseEntity<List<Cliente>>(lista, HttpStatus.OK);
	}
	
	
	@GetMapping(value = "/obtenerFoto/{id}", produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
	public ResponseEntity<byte[]> obtenerFoto(@PathVariable("id") Integer id) {
		Cliente c = service.listarPorId(id);
		byte[] data = c.getFoto();
		return new ResponseEntity<byte[]>(data, HttpStatus.OK);
	}
	
	@PostMapping
	public Usuario registrar(@RequestPart("usuario") Usuario usuario, @RequestPart("file") MultipartFile file)
			throws IOException {
		Usuario c = usuario;		
		c.getCliente().setFoto(file.getBytes());		
		//return usuarioIService.registrar(c);
		return usuarioIService.registrarTransaccional(c); 
	}

	@PutMapping
	public Usuario modificar(@RequestPart("usuario") Usuario usuario, @RequestPart("file") MultipartFile file)
			throws IOException {
		Usuario c = usuario;
		c.getCliente().setFoto(file.getBytes());
		return usuarioIService.modificar(c);
	}
	
	@DeleteMapping("/{id}")
	public ResponseEntity<Object> elminar(@PathVariable("id") Integer id){
		Cliente obj = service.listarPorId(id);
		if(obj.getIdCliente() == null) {
			throw new ModeloNotFoundException("ID NO EXISTE: " + id);
		}
		service.eliminar(id);
		return new ResponseEntity<Object>(HttpStatus.OK);
	}
	
	/*
	@GetMapping("/{id}")
	public ResponseEntity<Cliente> listarPorId(@PathVariable("id") Integer id){
		Cliente obj = service.listarPorId(id);
		if(obj.getIdCliente() == null) {
			throw new ModeloNotFoundException("ID NO EXISTE: " + id);
		}
		return new ResponseEntity<Cliente>(obj, HttpStatus.OK);
	}
	*/
	
	/*@PostMapping
	public ResponseEntity<Cliente> registrar(@RequestBody Cliente obj) {
		Cliente objeto = service.registrar(obj);
		return new ResponseEntity<Cliente>(objeto, HttpStatus.CREATED);
	}*/

	/*
	@PostMapping
	public ResponseEntity<Object> registrar(@Valid @RequestBody Cliente obj) {
		Cliente pel = service.registrar(obj);
		
		// localhost:8080/clientes/2
		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(pel.getIdCliente()).toUri();
		return ResponseEntity.created(location).build();
	}
	
	@PutMapping
	public ResponseEntity<Cliente> modificar(@Valid @RequestBody Cliente obj) {
		Cliente objeto = service.modificar(obj);
		return new ResponseEntity<Cliente>(objeto, HttpStatus.OK);
	}
	*/
	
	
}
